# 可拖拽甘特图

#### 介绍
采用vue2
在y轴上添加了虚拟列表，解决数据过多时候页面卡顿问题

#### 文件结构说明
```
src--
    common -- 一些通用的scss 和 js
    components -- 通用组件包
        context-menu -- 右键下拉菜单
        gantt -- 甘特图通用组件
    fake-data
        airPlaneData.js -- 伪造的数据
    pages
        gantt-index.vue -- 具体的页面
```

#### 项目截图

![图片1](src/assets/intr01.png)
![图片2](src/assets/intr02.png)
![图片3](src/assets/intr03.png)

