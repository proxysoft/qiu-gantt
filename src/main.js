import Vue from 'vue'
import App from './App.vue'
import dateFormat from "@/config/dateFormat.js"

Vue.config.productionTip = false
Vue.use(dateFormat)
new Vue({
  render: h => h(App),
}).$mount('#app')
