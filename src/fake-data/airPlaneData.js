const airPlaneData = [
    {
        "id": "1",
        "code": "11",
        "type": "A320",
        "layout": "",
        "timeBlock": [
            {
                "id": "991",
                "startTime": "2021/07/29 4:50:24",
                "endTime": "2021/07/29 6:50:24"
            },
            {
                "id": "992",
                "startTime": "2021/07/29 5:50:24",
                "endTime": "2021/07/29 8:50:24"
            },
            {
                "id": "993",
                "startTime": "2021/07/29 7:30:24",
                "endTime": "2021/07/29 8:20:24"
            },
        ]
    },
    {
        "id": "2",
        "code": "12",
        "type": "A320",
        "layout": "",
        "timeBlock": [

            {
                "id": "994",
                "startTime": "2021/07/29 12:30:24",
                "endTime": "2021/07/29 15:30:24"
            }
        ]
    },
    {
        "id": "3",
        "code": "16",
        "type": "A320",
        "layout": "",
        "timeBlock": []
    },
    {
        "id": "4",
        "code": "17",
        "type": "A320",
        "layout": "",
        "timeBlock": []
    },
    {
        "id": "5",
        "code": "14",
        "type": "A320",
        "layout": "",
        "timeBlock": []
    },
    {
        "id": "6",
        "code": "19",
        "type": "A320",
        "layout": "",
        "timeBlock": []
    },
    {
        "id": "7",
        "code": "20",
        "type": "A320",
        "layout": "",
        "timeBlock": []
    },
    {
        "id": "8",
        "code": "21",
        "type": "A320",
        "layout": "",
        "timeBlock": []
    },
    {
        "id": "9",
        "code": "21",
        "type": "A320",
        "layout": "",
        "timeBlock": []
    },
    {
        "id": "10",
        "code": "21",
        "type": "A320",
        "layout": "",
        "timeBlock": []
    },
    {
        "id": "11",
        "code": "21",
        "type": "A320",
        "layout": "",
        "timeBlock": []
    },
    {
        "id": "12",
        "code": "21",
        "type": "A320",
        "layout": "",
        "timeBlock": []
    },
    {
        "id": "13",
        "code": "21",
        "type": "A320",
        "layout": "",
        "timeBlock": []
    },
    {
        "id": "14",
        "code": "21",
        "type": "A320",
        "layout": "",
        "timeBlock": []
    },
    {
        "id": "15",
        "code": "21",
        "type": "A320",
        "layout": "",
        "timeBlock": [
            {
                "id": "995",
                "startTime": "2021/07/29 4:50:24",
                "endTime": "2021/07/29 6:50:24"
            },
            {
                "id": "996",
                "startTime": "2021/07/29 5:50:24",
                "endTime": "2021/07/29 8:50:24"
            },
            {
                "id": "997",
                "startTime": "2021/07/29 7:30:24",
                "endTime": "2021/07/29 8:20:24"
            },
        ]
    },
    {
        "id": "16",
        "code": "21",
        "type": "A320",
        "layout": "",
        "timeBlock": []
    },
    {
        "id": "17",
        "code": "21",
        "type": "A320",
        "layout": "",
        "timeBlock": []
    },
    {
        "id": "18",
        "code": "21",
        "type": "A320",
        "layout": "",
        "timeBlock": []
    },
    {
        "id": "19",
        "code": "21",
        "type": "A320",
        "layout": "",
        "timeBlock": []
    },
    
]
export default airPlaneData;
