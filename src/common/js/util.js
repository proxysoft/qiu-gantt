import Vue from 'vue'

// 事件总线
export const EventBus = new Vue()



// 获取盒子到屏幕上方的距离
export const getAbsTop = (dom) => {
    let top = dom.offsetTop;
    while (dom.offsetParent != null) {

        dom = dom.offsetParent;
        top += dom.offsetTop;
    }
    return top;
}

// 获取盒子到屏幕左方的距离
export const getAbsLeft = (dom) =>{
    let left = dom.offsetLeft;
    while (dom.offsetParent != null) {

        dom = dom.offsetParent;
        left += dom.offsetLeft;
    }
    return left;
}