export default {

    computed: {
        // isVirtual 为 true 展示
        // 用于虚拟列表展示的ganttData
        virtualGanttData() {

            let virtualList = this.ganttData.filter(e => e.isVirtual == true)

            if (virtualList.length === 0) {
                // 随便放一个，让他具有双向绑定的特性，就是个傀儡值
                return this.ganttData.slice(0, 1)
            }

            let virtualShowList = this.ganttData.filter(e => e.isVirtual == true)

            // 虚拟列表中用于展示的部分
            return virtualShowList
        },
        // 每一行的位置
        getRowPosition() {
            let offsetTopArr = []
            const showArr = this.ganttData.filter(e => !e.isHidden)
            for (let i = 0; i < showArr.length; i++) {
                if (i === 0) {
                    offsetTopArr.push(0)
                } else {
                    //上一个高度
                    const lastHeight = offsetTopArr[i - 1]
                    //当前高度
                    const currentHeight = showArr[i].height
                    offsetTopArr.push(lastHeight + currentHeight)
                }
            }
            return offsetTopArr
        },
        // ganttData 中 isHidden 为 false的数据
        noHiddenGanttData() {
            return this.ganttData.filter(e => !e.isHidden)
        },
        // 甘特图的长度
        ganttHeight() {
            const allHeight = this.ganttData.filter(e => !e.isHidden).map(e => e.height)
            //总的高度
            let sum = allHeight.reduce(function (prev, cur) {
                return prev + cur;
            }, 0);
            return sum
        },
        // 可视数据的高度
        viewHeight() {
            const viewHeight = this.virtualGanttData.map(e => e.height)

            //可视高度
            let viewSum = viewHeight.reduce(function (prev, cur) {
                return prev + cur;
            }, 0);

            return viewSum || 0
        },
    },
    data() {
        return {
            //用right 的滚动，控制aside-left的滚动
            transFormYValue: 0,
            //用right 的滚动，控制head-left的滚动
            transFormXValue: 0,

            // 占位
            virtual: {
                paddingTop: 0,
                paddingBottom: 0
            }
        }
    },
    methods: {
        //滚动事件
        getScrollEvent() {
            let rightDom = this.$refs.contentGrid
            rightDom.addEventListener("scroll", (e) => {

                //滚动的时候菜单栏消失
                this.rightMenu.isShow = false

                let scrollLeft = e.target.scrollLeft
                let scrollTop = e.target.scrollTop
                this.transFormYValue = scrollTop
                this.transFormXValue = scrollLeft

                this.getVirtualList()

            })

            //左边滚动
            let leftDom = this.$refs.left
            leftDom.addEventListener("mousewheel", (e) => {
                this.rightMenu.isShow = false
                let wheelValue = e.deltaY
                rightDom.scrollTop += wheelValue
            })
        },
        getVirtualList() {
            const rightDom = this.$refs.contentGrid

            // 外框的高度
            const outerHeight = rightDom.clientHeight

            const domScrollTop = rightDom.scrollTop

            let startIndex = 0

            let endIndex = 0

            //虚假的占位设置
            //顶部的距离位置

            const sum = this.ganttHeight

            //const viewSum = this.viewHeight

            //所有行的位置
            const rowHeightArr = Array.from(new Set(this.getRowPosition))

            // 得到开始索引的位置
            for (let i = 0; i < rowHeightArr.length; i++) {
                let itemV = rowHeightArr[i]
                if (domScrollTop > itemV) {
                    startIndex = i
                }
            }

            // 当前结束所在的高度
            let endHeight = outerHeight + domScrollTop


            // 得到结束的那行索引
            for (let i = 0; i < rowHeightArr.length; i++) {
                let itemV = rowHeightArr[i]
                if (endHeight > itemV) {
                    endIndex = i
                }
            }

            //可见区域的数据
            let viewRowData = this.noHiddenGanttData.slice(startIndex, endIndex + 1)

            //复原
            for (let i = 0; i < this.ganttData.length; i++) {
                this.ganttData[i].isVirtual = false
            }

            // 给viewRowData 添加isVirtual 属性
            for (let i = 0; i < viewRowData.length; i++) {
                viewRowData[i].isVirtual = true
            }

            const showAll = this.noHiddenGanttData.map(e => e.height)

            // 距离顶部的高度 计算padding
            const topHeight = showAll.slice(0, startIndex).reduce((pre, cur) => {
                return pre + cur
            }, 0)

            const realViewSum = this.viewHeight

            // 距离底部的高度
            const bottomHeight = sum - topHeight - realViewSum

            this.virtual.paddingTop = topHeight

            this.virtual.paddingBottom = bottomHeight

        }
    }
}